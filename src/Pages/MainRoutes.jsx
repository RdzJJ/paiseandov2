import React from "react";
import { Route, Routes } from "react-router-dom";
import Homepage from "./Homepage";
import { Hotels } from "../Components/Hotels/Hotels";
import SinglePage from "../Components/Activities/SinglePage";
import Things from "../Components/Activities/Things";
import { Restaurants } from "../Components/Restaurants/Restaurants";

const MainRoutes = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/hotels" element={<Hotels />} />
        <Route path="/restaurants" element={<Restaurants />} />
        <Route path="/places/:id" element={<SinglePage />} />
        <Route path="/things" element={<Things />} />
      </Routes>
    </>
  );
};

export default MainRoutes;
