import React from "react";
import Footer from "../Components/Footer/Footer";
import Navbar from "../Components/Navbar/Navbar";
import homepage from "./homepage.module.css";
import Carousel32 from "../Components/Carousal/Carousal32";
import OuterNav from "../Components/Outernav/OuterNav";


const Homepage = () => {
  return (
    <>
      <Navbar />
      <OuterNav />
      <div>
        <div className={homepage.hdiv1} style={{ backgroundImage: "url(/search_bg.jpg)" }}>
          <div className={homepage.search}>
            <div className={homepage.div_flex}>
              <img
                className={homepage.search_logo}
                src={process.env.PUBLIC_URL + "/images/search_icon.png"}
                alt=""
              />
              <div className={homepage.search_input}>Que queieres Hacer?</div>
            </div>
          </div>
        </div>
      </div>

      {/* //top destination for your next */}
      <div className={homepage.way_nagpur}>
        <div className={homepage.ways_to2}>
          Destinos Top para tus próximas aventuras
        </div>
        <div className={homepage.carousal}>{<Carousel32 />}</div>
      </div>
      <Footer />
    </>
  );
};

export default Homepage;
