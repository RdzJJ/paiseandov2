import { NavLink } from "react-router-dom";
import "./InnerNav.css";
import { innerNavItems } from "./InnerNavItems";

const InnerNav = () => {
  const activeStyle = {
    borderBottom: "3px solid black",
    fontWeight: "bold",
    textDecoration: "none",
    paddingBottom: "0.5rem",
  };
  const style = {
    color: "#333",
    textDecoration: "none",
  };

  return (
    <>
      <ul className="nav-items">
        {innerNavItems.map((items) => {
          return (
            <li key={items.id} className={items.cName}>
              <NavLink
                style={({ isActive }) => (isActive ? activeStyle : style)}
                to={items.path}
              >
                {items.title}
              </NavLink>
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default InnerNav;
