export const innerNavItems = [
  {
    id: 2,
    title: "Hoteles",
    path: "/hotels",
    cName: "nav-item",
  },
  {
    id: 3,
    title: "Lugares Para Visitar",
    path: "/things",
    cName: "nav-item",
  },
  {
    id: 4,
    title: "Restaurantes",
    path: "/restaurants",
    cName: "nav-item",
  },
  {
    id: 3,
    title: "...",
    path: "/",
    cName: "nav-item",
  },
];