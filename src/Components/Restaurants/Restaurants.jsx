import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getRestuarantList } from "../../Redux/AppReducer/action";
import styles from "./Restaurants.module.css";
import InnerNav from "../InnerNav/InnerNav";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";

const Restaurants = () => {
  let dispatch = useDispatch();
  const Reslist = useSelector((store) => store.AppReducer.restaurantList);
  const [sort, setSort] = useState("");

  const handleChange = (e) => {
    e.preventDefault();
    setSort(e.target.value);

    if (e.target.value === "lth") {
      Reslist.sort((a, b) => {
        return Number(a.rating) - Number(b.rating);
      });
    } else if (e.target.value === "htl") {
      Reslist.sort((a, b) => {
        return Number(b.rating) - Number(a.rating);
      });
    }
  };

  useEffect(() => {
    dispatch(getRestuarantList);
  }, [dispatch]);

  console.log(sort);
  return (
    <>
      <Navbar />
      <InnerNav />
      <div className={styles.container}>
        <div className={styles.top}>
          <p>Restaurantes en Medellin</p>
        </div>
        <div className={styles.buttomDiv}>
          <div className={styles.resDiv}>
            <div className={styles.sorting}>
              <label>Ordenar Por: </label>
              <select onChange={handleChange}>
                <option>sort By</option>
                <option value="lth">Calification(Baja a Alta)</option>
                <option value="htl">Calification(Alta a Baja)</option>
              </select>
            </div>
            <div className={styles.resListingDiv}>
              {Reslist.map((rest, ind) => {
                return (
                  <div key={rest.id} className={styles.res}>
                    <div className={styles.resPic}>
                      <img
                        src={rest.srcImg}
                        className={styles.imgRes}
                        alt="hotel_photo"
                      />
                    </div>
                    <div className={styles.aboutResDiv}>
                      <div className={styles.ResName}>
                        {ind + 1}.{rest.name}
                      </div>
                      <div className={styles.RatePicDiv}>
                        {rest.ratePic.map((ele) => (
                          <img
                            src={ele.src}
                            key={ele.id}
                            alt="rate-pic"
                            className={styles.ratingPic}
                          />
                        ))}
                      </div>
                      <div className={styles.typeDiv}>
                        <p>{rest.type}</p>
                      </div>
                      <div className={styles.resDetail}>
                        <div className={styles.description}>
                          <p className={styles.review}>"{rest.comment}"</p>
                          <button className={styles.button}>Reservar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export { Restaurants };
