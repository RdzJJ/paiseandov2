import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Img } from "@chakra-ui/react";
import a1 from "../../Components/SignIn/images/1.PNG";
import a2 from "../../Components/SignIn/images/2.PNG";
import a3 from "../../Components/SignIn/images/3.PNG";
import a4 from "../../Components/SignIn/images/4.PNG";
import a5 from "../../Components/SignIn/images/5.PNG";
import a6 from "../../Components/SignIn/images/6.PNG";
import a7 from "../../Components/SignIn/images/7.PNG";
import a9 from "../../Components/SignIn/images/9.PNG";

const Carousal32 = () => {
  var settings = {
    dots: true,
    arrow: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    gap: "10px",
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div style={{ margin: "40px" }}>
      <Slider {...settings}>
        <div>
          <Img src={a1} />
        </div>
        <div>
          <Img src={a2} />
        </div>
        <div>
          <Img src={a3} />
        </div>
        <div>
          <Img src={a4} />
        </div>
        <div>
          <Img src={a5} />
        </div>
        <div>
          <Img src={a6} />
        </div>
        <div>
          <Img src={a7} />
        </div>
        <div>
          <Img src={a9} />
        </div>
      </Slider>
    </div>
  );
};

export default Carousal32;
