import React from "react";
import { useEffect } from "react";
import styles from "./Hotels.module.css";
import { useDispatch, useSelector } from "react-redux";
import { getHotelList, getSightList } from "../../Redux/AppReducer/action";
import InnerNav from "../InnerNav/InnerNav";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";

const Hotels = () => {
  let dispatch = useDispatch();
  const list = useSelector((store) => store.AppReducer.hotelList);
  const sightList = useSelector((store) => store.AppReducer.sight);

  const [sort, setSort] = React.useState("");

  const handleChange = (e) => {
    e.preventDefault();
    setSort(e.target.value);

    if (e.target.value === "lthr") {
      list.sort((a, b) => {
        return Number(a.rating) - Number(b.rating);
      });
    } else if (e.target.value === "htlr") {
      list.sort((a, b) => {
        return Number(b.rating) - Number(a.rating);
      });
    }
    if (e.target.value === "lthp") {
      list.sort((a, b) => {
        return Number(a.price) - Number(b.price);
      });
    } else if (e.target.value === "htlp") {
      list.sort((a, b) => {
        return Number(b.price) - Number(a.price);
      });
    }
  };
  useEffect(() => {
    dispatch(getHotelList);
    dispatch(getSightList);
  }, [dispatch]);

  console.log(sort);

  return (
    <>
      <Navbar />
      <InnerNav />
      <div className={styles.container}>
        <div className={styles.container}>
          <div className={styles.top}>
            <p className={styles.heading}>Hoteles y lugares para quedarse</p>
            <p className={styles.subheading}>
              Introduce una fecha para encontrar los mejores precios.
            </p>
            <div className={styles.calenderDiv}>
              <div className={styles.outerDiv1}>
                <div className={styles.innerDiv1}>
                  <input type="date" className={styles.checkIn} />
                </div>
              </div>
              <div className={styles.outerDiv2}>
                <div className={styles.innerDiv2}>
                  <input type="date" className={styles.checkIn} />
                </div>
              </div>
              <div className={styles.outerDiv3}>
                <div className={styles.innerDiv3}>
                  <p>Huésped 1 habitación, 2 adultos, 0 niños</p>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.buttomDiv}>
            <div className={styles.hotelsDiv}>
              <div className={styles.sorting}>
                <label>Sort By: </label>
                <select onChange={handleChange}>
                  <option>sort By</option>
                  <option value="lthr">Calification(Baja a Alta)</option>
                  <option value="htlr">Calification(Alta a Baja)</option>
                  <option value="lthp">Precio(Bajo a Alto)</option>
                  <option value="htlp">Precio(Alto a Bajo)</option>
                </select>
              </div>
              <div className={styles.hotelsListingDiv}>
                {list.map((hotel, ind) => {
                  return (
                    <div key={hotel.id} className={styles.hotel}>
                      <div className={styles.hotelPic}>
                        <img
                          src={hotel.imageSrc}
                          className={styles.imgHotel}
                          alt="hotel_photo"
                        />
                      </div>
                      <div className={styles.aboutHotelDiv}>
                        <div className={styles.hotelName}>
                          {ind + 1}.{hotel.name}
                        </div>
                        <div className={styles.hotelDetail}>
                          <div className={styles.suggestion}>
                            <img
                              src={hotel.best_site}
                              className={styles.site}
                              alt="best-site_logo"
                            />
                            <p>₹{hotel.price}</p>
                            <button className={styles.button}>
                              <a
                                href={hotel.href}
                                target="_blank"
                                rel="noreferrer"
                              >
                                Ver Ofertas
                              </a>
                            </button>
                          </div>
                          <div className={styles.bestOptions}></div>
                          <div className={styles.description}>
                            <div className={styles.RatePicDiv}>
                              {hotel.ratePic.map((ele) => (
                                <img
                                  src={ele.src}
                                  key={ele.id}
                                  alt="rate-pic"
                                  className={styles.ratingPic}
                                />
                              ))}
                              <p className={styles.reviews}>{hotel.reviews}</p>
                            </div>
                            <p className={styles.aboutHead}>About</p>
                            <p>{hotel.about}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div className={styles.sights}>
          <p className={styles.sightHead}>Hoteles cercanos</p>
          <div className={styles.sightDiv}>
            {sightList.map((sight) => (
              <div key={sight.id} className={styles.sightInd}>
                <img
                  src={sight.srcImg}
                  className={styles.imgSight}
                  alt="sight-pic"
                />
                <p>{sight.name}</p>
              </div>
            ))}
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export { Hotels };
