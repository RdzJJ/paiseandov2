import React from "react";
import footer from "./footer.module.css";

const Footer = () => {
  return (
    <div className={footer.b_Footer}>
      <div className={footer.footer_2}>
        <div className={footer.logo}>
          {" "}
          <img
            src={process.env.PUBLIC_URL + "/favicon.svg"}
            alt=""
          />
        </div>
        <div>
          <div className={footer.rights}>
            <div>2024 Paiseando LLC All rights reserved.</div>
          </div>
          <div
            style={{ display: "flex", margin: "auto" }}
            className={footer.Term_of_use}
          >
            <div>Terms of Use</div>
            <div>Privacy and Cookies Statement</div>
            <div>Cookie consent</div>
            <div>Site Map</div>
            <div>How the site Works.</div>
          </div>
        </div>
      </div>
      <div>3</div>

      {/* bfooter */}
    </div>
  );
};

export default Footer;
