import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Text,
} from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/react";
import navbar from "../Navbar/navbar.module.css";
import modal from "./modal.module.css";

const Modal1 = ({ SetSignIn }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Button
        onClick={onOpen}
        className={navbar.signin_Button}
        colorScheme="Black "
        variant="solid"
        br="20px"
      >
        Iniciar Sesión
      </Button>

      <Modal blockScrollOnMount={false} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader></ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text mb="1rem">
              <div className={module.m1_main}>
                <div>
                  <img
                    className={modal.m1_logo}
                    src={process.env.PUBLIC_URL + "/banner.svg"}
                    alt=""
                  />
                </div>
                <div className={modal.m11}>Inicia Sesión para</div>
                <div className={modal.m11}>Disfrutar de Paiseando</div>
                <div className={modal.m1o}>
                  <div className={modal.m1G}>
                    <img
                      className={modal.m1_elogo}
                      src="https://th.bing.com/th/id/OIP.eSrmTw44QXD79PzHo4CcngAAAA?pid=ImgDet&w=300&h=300&rs=1"
                      alt=""
                    />
                    Continuar con Correo
                  </div>
                </div>
                {/* Boton Olvidaste tu contraseña */}
                <div className={modal.m1_forgot}>
                  <button className={modal.m1_forgot_button}>
                    ¿Olvidaste tu contraseña?
                  </button>
                </div>
              </div>
            </Text>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Modal1;
