import React, { useEffect } from "react";
import styles from "./Things.module.css";
import Slides from "./Slides";
import { Heading, Text } from "@chakra-ui/react";
import Slide from "./Slide";
import { useDispatch, useSelector } from "react-redux";
import { placesdata } from "../../Redux/AppReducer/action";
import TopAttraction from "./Top_Attraction";
import { Link } from "react-router-dom";
import PlacesCard from "./PlacesCard";
import InnerNav from "../InnerNav/InnerNav";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";

const Things = () => {
  const dispatch = useDispatch();

  const places = useSelector((state) => state.AppReducer.places);

  useEffect(() => {
    dispatch(placesdata());
  }, [dispatch]);
  console.log(places);
  return (
    <>
      <Navbar />
      <InnerNav />
      <div style={{ width: "85%", alignItems: "center", margin: "auto" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p
            style={{
              fontSize: "42px",
              fontWeight: "700",
              lineHeight: "50px",
              marginTop: "50px",
              color: "black",
            }}>
            Lugares Para Visitar
          </p>
          <button
            style={{
              width: "80px",
              marginTop: "50px",
              color: "white",
              padding: "10px",
              borderRadius: "10px",
              background: "black",
            }}
          >
            Mapa
          </button>
        </div>
        <br />
        <br />
        <div>
          <p style={{ fontSize: "24px", textAlign: "left", fontWeight: "700" }}>
            Sitios turisticos mas populares
          </p>
          <br />
        </div>
        <Slides />
        <hr />
        <div>
          <Heading>Date un tour por Medellin</Heading>
          <Text> Reserva estas experiencias para conocer Medellín.</Text>
          <Slide />
          <hr />
        </div>
        <div>
          <Heading>Recomendado Para ti</Heading>
          <Text> Tours y actividades adaptadas a tus intereses.</Text>
          <div className={styles.place}>
            {places?.length > 0 &&
              places?.map((el) => (
                <Link to={`/places/${el.id}`} key={el.id}>
                  <PlacesCard key={el.id} item={el} />
                </Link>
              ))}
          </div>
        </div>
        <hr />
        {/* Top Attraction */}
        <TopAttraction />
        <br />
        <br />
      </div>
      <Footer />
    </>
  );
};
export default Things;
