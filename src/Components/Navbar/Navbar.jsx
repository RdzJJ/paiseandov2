import React, { useEffect, useState } from "react";
import Modal1 from "../SignIn/Modal1";
import Modal2 from "../SignIn/Modal2";
import DropDown from "../SignIn/DropDown";

import navbar from "../Navbar/navbar.module.css";
import { Link } from "react-router-dom";

import { useDispatch } from "react-redux/es/exports";
import { getAuthSucc } from "../../Redux/AuthReducer/action";

const Navbar = () => {
  const dispatch = useDispatch();
  const [SignIn, SetSignIn] = useState(0);
  const [dropdown_nav, setDropDown_nav] = useState(1);
  useEffect(() => {
    if (dropdown_nav) {
      dispatch(getAuthSucc());
    }
  }, [dropdown_nav, dispatch]);
  console.log(SignIn);

  return (
    <>
      <div className={navbar.navbarDiv}>
        <Link to="/">
          <div>
            <img
              className={navbar.logo}
              src={process.env.PUBLIC_URL + '/banner.svg'}
              alt=""
            />
          </div>
        </Link>
        <div className={navbar.divright2}>
          <div className={navbar.flex_logo}>
            <div className={navbar.logo_img}>
              <img
                // reviews icon in public/images folder
                src={process.env.PUBLIC_URL + '/images/reviews_icon.png'}
                alt=""
              />
            </div>
            Reseñas
          </div>

          <div className={navbar.flex_logo}>
            <div className={navbar.logo_img}>
              <img
                src={process.env.PUBLIC_URL + '/images/favorites_icon.png'}
                alt=""
              />
            </div>
            Favoritos
          </div>
          <div className={navbar.flex_logo} id={navbar.login}>
            <div className={navbar.logo_img}>
              <img
                src={process.env.PUBLIC_URL + '/images/notifications_icon.jpg'}
                alt=""
              />
            </div>
            Notificaciones
          </div>

          <div>
            {dropdown_nav ? (
              SignIn ? (
                <Modal2 setDropDown_nav={setDropDown_nav} />
              ) : (
                <Modal1 SetSignIn={SetSignIn} />
              )
            ) : (
              <DropDown
                setDropDown_nav={setDropDown_nav}
                SetSignIn={SetSignIn}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
